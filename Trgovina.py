from tkinter import *

import requests
import re
import urllib
from PIL import Image
from io import BytesIO
from io import StringIO
from tkinter import *
import io
import base64
import urllib.request
from urllib.request import urlopen
from PIL import Image, ImageTk

class Trgovina():
    def izracunaj(self, *argumenti):

        s1 = {'€':1, '$':0.846763, '£':1.27980}
        s2 = {'EUR':1, 'USD':1.18097, 'GBP':0.781367}
        s3 = {'EUR':'€', 'USD':'$', 'GBP':'£'}
        
        n = self.iskanje.get()

####### AMAZON #######
        
        r_a = requests.get('http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords={0}'.format(n))
        a_a = re.findall(r"""<li id="result_\d+".*?</li>""", r_a.text, flags=re.DOTALL)

        s_a = []
        s_ai = []
        for i in a_a:
            if '</span>new' in i:
                b = re.findall(r"""<span class="a-size-base a-color-price a-text-bold">\$.*?</span><span class="a-letter-space"></span>new""", i, flags=re.DOTALL)
                b[0] = b[0].replace('<span class="a-size-base a-color-price a-text-bold">', '')
                b[0] = b[0].replace('</span><span class="a-letter-space"></span>new', '')
                b[0] = b[0].replace(',', '')
                s_a.append(b[0])
                d = re.findall(r"""http://ecx\.images-amazon\.com/images/I/.*?\._AA160_.jpg""", i)
                if len(d) > 0:
                    d[0] = d[0].replace('src="', '')
                    s_ai.append(d[0])
                else:
                    f = 'http://www.acsu.buffalo.edu/~rslaine/imageNotFound.jpg'
                    s_ai.append(f)
            elif '</span>used &amp; new' in i:
                b = re.findall(r"""<span class="a-size-base a-color-price a-text-bold">\$.*?</span><span class="a-letter-space"></span>used &amp; new""", i, flags=re.DOTALL)
                b[0] = b[0].replace('<span class="a-size-base a-color-price a-text-bold">', '')
                b[0] = b[0].replace('</span><span class="a-letter-space"></span>used &amp; new', '')
                b[0] = b[0].replace(',', '')
                s_a.append(b[0])
                d = re.findall(r"""http://ecx\.images-amazon\.com/images/I/.*?\._AA160_.jpg""", i)
                if len(d) > 0:
                    d[0] = d[0].replace('src="', '')
                    s_ai.append(d[0])
                else:
                    f = 'http://www.acsu.buffalo.edu/~rslaine/imageNotFound.jpg'
                    s_ai.append(f)

        if len(s_a) == 0:
            vrednost1 = 0
            vrednost2 = 0
            vrednost3 = 0
        elif len(s_a) == 1:
            vrednost1 = float(s_a[0][1:])
            vrednost2 = 0
            vrednost3 = 0
        elif len(s_a) == 2:
            vrednost1 = float(s_a[0][1:])
            vrednost2 = float(s_a[1][1:])
            vrednost3 = 0
        else:
            vrednost1 = float(s_a[0][1:])
            vrednost2 = float(s_a[1][1:])
            vrednost3 = float(s_a[2][1:])
            
        v1 = '{0:.2f}'.format(vrednost1 * s1['$'] * s2[self.valuta.get()])
        v2 = '{0:.2f}'.format(vrednost2 * s1['$'] * s2[self.valuta.get()])
        v3 = '{0:.2f}'.format(vrednost3 * s1['$'] * s2[self.valuta.get()])

        self.cena1.set(v1 + ' ' + s3[self.valuta.get()])
        self.cena2.set(v2 + ' ' + s3[self.valuta.get()])
        self.cena3.set(v3 + ' ' + s3[self.valuta.get()])

        if len(s_ai) == 0:
            j1 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j2 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j3 = 'http://www.lugaa.com/public/images/noproject.jpg'
        elif len(s_ai) == 1:
            j1 = s_ai[0]
            j2 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j3 = 'http://www.lugaa.com/public/images/noproject.jpg'
        elif len(s_ai) == 2:
            j1 = s_ai[0]
            j2 = s_ai[1]
            j3 = 'http://www.lugaa.com/public/images/noproject.jpg'
        else:
            j1 = s_ai[0]
            j2 = s_ai[1]
            j3 = s_ai[2]

        url1 = j1
        link1 = urllib.request.urlopen(url1)
        image_bytes1 = urlopen(url1).read()
        data1 = io.BytesIO(image_bytes1)
        pil_image1 = Image.open(data1)
        pil_image1 = pil_image1.resize((150, 150), Image.ANTIALIAS)
        tk_image1 = ImageTk.PhotoImage(pil_image1)
        self.label1.config(image=tk_image1, bg="#99004C")

        url2 = j2
        link2 = urllib.request.urlopen(url2)
        image_bytes2 = urlopen(url2).read()
        data2 = io.BytesIO(image_bytes2)
        pil_image2 = Image.open(data2)
        pil_image2 = pil_image2.resize((150, 150), Image.ANTIALIAS)
        tk_image2 = ImageTk.PhotoImage(pil_image2)
        self.label2.config(image=tk_image2, bg="#99004C")

        url3 = j3
        link3 = urllib.request.urlopen(url3)
        image_bytes3 = urlopen(url3).read()
        data3 = io.BytesIO(image_bytes3)
        pil_image3 = Image.open(data3)
        pil_image3 = pil_image3.resize((150, 150), Image.ANTIALIAS)
        tk_image3 = ImageTk.PhotoImage(pil_image3)
        self.label3.config(image=tk_image3, bg="#99004C")

####### eBAY #######       

        r_e = requests.get('http://www.ebay.com/sch/i.html?_from=R40&_trksid=p2050601.m570.l1313.TR0.TRC0.H0.X{0}&_nkw={1}&_sacat=0'.format(n, n))
        a_e = re.findall(r"""<li id="item.*?<li id="item""", r_e.text, flags=re.DOTALL)

        s_e = []
        s_ei = []
        
        for i in a_e:
            if 'Buy It Now</span>' in i:
                b = re.findall(r"""\$.*?</span>.*?Buy It Now</span>""", i, flags=re.DOTALL)
                for j in b:
                    if 'javascript' in j:
                        continue
                    else:
                        c = re.findall(r"""\$.*?\</span>""", j[-100:], flags=re.DOTALL)
                        c[-1] = c[-1].replace('</span>', '')
                        c[-1] = c[-1].replace(',', '')
                        s_e.append(c[-1])
                        d = re.findall(r"""src="http://.*?\.jpg""", i, flags=re.DOTALL)
                        if len(d) > 0:
                            if 'imgurl' in d[0]:
                                e = d[0].index('imgurl')
                                d[0] = d[0][e:]
                            d[0] = d[0].replace('src="', '')
                            d[0] = d[0].replace('imgurl="', '')
                            s_ei.append(d[0])
                        else:
                            f = 'http://www.acsu.buffalo.edu/~rslaine/imageNotFound.jpg'
                            s_ei.append(f)
                        

        if len(s_e) == 0:
            vrednost4 = 0
            vrednost5 = 0
            vrednost6 = 0
        elif len(s_e) == 1:
            vrednost4 = float(s_e[0][1:])
            vrednost5 = 0
            vrednost6 = 0
        elif len(s_e) == 2:
            vrednost4 = float(s_e[0][1:])
            vrednost5 = float(s_e[1][1:])
            vrednost6 = 0
        else:
            vrednost4 = float(s_e[0][1:])
            vrednost5 = float(s_e[1][1:])
            vrednost6 = float(s_e[2][1:])
            
        v4 = '{0:.2f}'.format(vrednost4 * s1['$'] * s2[self.valuta.get()])
        v5 = '{0:.2f}'.format(vrednost5 * s1['$'] * s2[self.valuta.get()])
        v6 = '{0:.2f}'.format(vrednost6 * s1['$'] * s2[self.valuta.get()])

        self.cena4.set(v4 + ' ' + s3[self.valuta.get()])
        self.cena5.set(v5 + ' ' + s3[self.valuta.get()])
        self.cena6.set(v6 + ' ' + s3[self.valuta.get()])

        if len(s_ei) == 0:
            j4 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j5 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j6 = 'http://www.lugaa.com/public/images/noproject.jpg'
        elif len(s_ei) == 1:
            j4 = s_ei[0]
            j5 = 'http://www.lugaa.com/public/images/noproject.jpg'
            j6 = 'http://www.lugaa.com/public/images/noproject.jpg'
        elif len(s_ei) == 2:
            j4 = s_ei[0]
            j5 = s_ei[1]
            j6 = 'http://www.lugaa.com/public/images/noproject.jpg'
        else:
            j4 = s_ei[0]
            j5 = s_ei[1]
            j6 = s_ei[2]

        url4 = j4
        link4 = urllib.request.urlopen(url4)
        image_bytes4 = urlopen(url4).read()
        data4 = io.BytesIO(image_bytes4)
        pil_image4 = Image.open(data4)
        pil_image4 = pil_image4.resize((150, 150), Image.ANTIALIAS)
        tk_image4 = ImageTk.PhotoImage(pil_image4)
        self.label4.config(image=tk_image4, bg="#99004C")

        url5 = j5
        link5 = urllib.request.urlopen(url5)
        image_bytes5 = urlopen(url5).read()
        data5 = io.BytesIO(image_bytes5)
        pil_image5 = Image.open(data5)
        pil_image5 = pil_image5.resize((150, 150), Image.ANTIALIAS)
        tk_image5 = ImageTk.PhotoImage(pil_image5)
        self.label5.config(image=tk_image5, bg="#99004C")

        url6 = j6
        link6 = urllib.request.urlopen(url6)
        image_bytes6 = urlopen(url6).read()
        data6 = io.BytesIO(image_bytes6)
        pil_image6 = Image.open(data6)
        pil_image6 = pil_image6.resize((150, 150), Image.ANTIALIAS)
        tk_image6 = ImageTk.PhotoImage(pil_image6)
        self.label6.config(image=tk_image6, bg="#99004C")


        raise ValueError

    def color_change(self):
        self.button.configure(bg="red")
    
    def __init__(self, root):

        root.title("Spletna trgovina")
        
        okvir = Frame(root, bg = "#FFFFCC")
        okvir.grid(column=1, row=1)

        self.iskanje = StringVar()
        
        self.cena1 = StringVar()
        self.cena2 = StringVar()
        self.cena3 = StringVar()
        
        self.cena4 = StringVar()
        self.cena5 = StringVar()
        self.cena6 = StringVar()

        self.valuta = StringVar()
        self.valuta.set("EUR")

        vnosno_polje = Entry(okvir, textvariable=self.iskanje)
        vnosno_polje.grid(column=2, row=1)

        Label(okvir, text="Spletna trgovina", font=('Helvetica',13), bg="#99004C", fg="#FFFFCC").grid(column=2, row=0)
        Label(okvir, text="Izdelek", font=('Helvetica',13), bg="#99004C", fg="#FFFFCC").grid(column=1, row=1)
        Label(okvir, text="Amazon", font=('Helvetica',11),bg="#99004C", fg="#FFFFCC").grid(column=2, row=2)
        Label(okvir, text="eBay", font=('Helvetica',11), bg="#99004C", fg="#FFFFCC").grid(column=3, row=2)
        
        Label(okvir, text="1. možnost", bg="#99004C", fg="#FFFFCC").grid(column=1, row=4)
        Label(okvir, text="2. možnost", bg="#99004C", fg="#FFFFCC").grid(column=1, row=7)
        Label(okvir, text="3. možnost", bg="#99004C", fg="#FFFFCC").grid(column=1, row=10)

        Label(okvir, width=25, bg="#FFFFCC").grid(column=1, row=0)
        Label(okvir, height=1, bg="#FFFFCC").grid(column=1, row=3)
        Label(okvir, height=1, bg="#FFFFCC").grid(column=1, row=6)
        Label(okvir, height=1, bg="#FFFFCC").grid(column=1, row=9)
        Label(okvir, height=10, bg="#FFFFCC").grid(column=1, row=5)
        Label(okvir, height=10, bg="#FFFFCC").grid(column=1, row=8)
        Label(okvir, height=10, bg="#FFFFCC").grid(column=1, row=11)

        Label(okvir, textvariable=self.cena1, bg="#FFFFCC", width=25).grid(column=2, row=4)
        Label(okvir, textvariable=self.cena2, bg="#FFFFCC").grid(column=2, row=7)
        Label(okvir, textvariable=self.cena3, bg="#FFFFCC").grid(column=2, row=10)

        Label(okvir, textvariable=self.cena4, bg="#FFFFCC", width=25).grid(column=3, row=4)
        Label(okvir, textvariable=self.cena5, bg="#FFFFCC").grid(column=3, row=7)
        Label(okvir, textvariable=self.cena6, bg="#FFFFCC").grid(column=3, row=10)

        self.label1 = Label(okvir)
        self.label1.grid(column=2, row=5)
        
        self.label2 = Label(okvir)
        self.label2.grid(column=2, row=8)
        
        self.label3 = Label(okvir)
        self.label3.grid(column=2, row=11)

        self.label4 = Label(okvir)
        self.label4.grid(column=3, row=5)
        
        self.label5 = Label(okvir)
        self.label5.grid(column=3, row=8)
        
        self.label6 = Label(okvir)
        self.label6.grid(column=3, row=11)
        
        m = OptionMenu(okvir, self.valuta, "EUR", "USD", "GBP")
        m.config(bg="#99004C", fg="#FFFFCC", activebackground="#CC0066",activeforeground="#FFFFCC")
        m.grid(column=3, row=0)
       
        Button(okvir, text="Iskanje!", font=('Helvetica',13),bg="#CC0066",fg="#FFFFCC",activebackground="#FF007F", activeforeground="#FFFFCC", command=self.izracunaj).grid(column=3, row=1)

        for otrok in okvir.winfo_children():
            otrok.grid_configure(padx=20, pady=5)

        root.bind("<Return>", self.izracunaj)
        vnosno_polje.focus()
        

master = Tk()
app = Trgovina(master)
master.mainloop()
