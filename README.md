# README #


### Čemu je program namenjen? ###

* primerjanju cen izdelkov v dveh spletnih trgovinah - Amazon in eBay

### Kako poženem program? ###

* Odprem datoteko *Trgovina-koncna.py*
* Ostala navodila za uporabo so napisana v datoteki *Porocilo - trgovina.pdf*


### Na koga se lahko obrnem za več informacij? ###

* Ana Borovac
* Mihaela Pušnik